package main.java.treeftp;

public class ErrorWhileCreatingSocketException extends Exception {
    public ErrorWhileCreatingSocketException(String errorMessage) {
        super(errorMessage);
    }
}