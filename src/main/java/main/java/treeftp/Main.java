package main.java.treeftp;

/***
 * @author  Nathan Houzet
 */
public class Main {

    public static void main(String[] args){
        String host = args[0];
        int port =21;
        String user;
        if (args.length==2){
            user = args[1];
        }else{
            user = "anonymous";
        }
        String password;
        if (args.length==3){
             password = args[2];
        }else{
            password = "";
        }


        try{
            Connexion ftpConnexion = new Connexion(host, port);

            FtpCommands ftp = new FtpCommands(user, password, ftpConnexion);

            ftp.LoginToFtpServer();
            ftp.ReceiveProcedure();
            ftp.Tree("-");



        }catch (Exception e){System.out.print("Error : ");System.out.println(e.getMessage());}

    }
}
