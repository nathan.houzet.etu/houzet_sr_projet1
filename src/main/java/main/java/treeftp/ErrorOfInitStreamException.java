package main.java.treeftp;

public class ErrorOfInitStreamException extends Exception {
    public ErrorOfInitStreamException(String errorMessage) {
        super(errorMessage);
    }
}