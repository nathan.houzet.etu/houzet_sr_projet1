package main.java.treeftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/***
 * Class that represent a TCP Connexion used to create a connection with the FTP server.
 * @author Nathan Houzet
 */
public class Connexion {
    String host;
    int port;
    Socket socket;

    /***
     * Constructor
     * @param host the ip address where u want to connect
     * @param port the port you want to use
     * @throws ErrorWhileCreatingSocketException if the connection with the Sockets fails
     */
    public Connexion(String host, int port) throws ErrorWhileCreatingSocketException {
        this.host=host;
        this.port =port;
        init(host, port);
    }

    /***
     *Create the socket with
     * @param host the ip address where u want to connect
     * @param port the port you want to use
     * @throws ErrorWhileCreatingSocketException IOException if the connection with the Sockets fails
     */
    public void init(String host, int port) throws ErrorWhileCreatingSocketException {
        try{
            this.socket =  new Socket(host,port);
        }
        catch(IOException e){
            throw new ErrorWhileCreatingSocketException("Error lors de la création de la socket");
        }
    }

    /***
     * Create the InputStream associated to the socket in properties of the class.
     * @return the InputStream associated to the socket in properties of the class.
     * @throws ErrorOfInitStreamException if the creation of the InputStream fails
     */
    public InputStream InitInputStream() throws  ErrorOfInitStreamException {
        try {
            return this.socket.getInputStream();
        }
        catch (IOException e){
            throw new ErrorOfInitStreamException("Error while creating input stream of socket");
        }
    }

    /***
     * Create the OutputStream associated to the socket in properties of the class.
     * @return the OutputStream associated to the socket in properties of the class.
     * @throws ErrorOfInitStreamException if the creation of the OutputStream fails
     */
    public OutputStream InitOutputStream() throws ErrorOfInitStreamException {
        try{
            return this.socket.getOutputStream();
        }
        catch (IOException e){
            throw new ErrorOfInitStreamException("Error while creating output stream of socket");
        }

    }
}
