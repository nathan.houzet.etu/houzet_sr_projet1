package main.java.treeftp;

public class ErrorOfSystemReadingException extends Exception {
    public ErrorOfSystemReadingException(String errorMessage) {
        super(errorMessage);
    }
}