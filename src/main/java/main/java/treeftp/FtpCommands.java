package main.java.treeftp;

import java.io.*;

/***
 * Class that is used to communicate with the ftp server
 * @author Nathan Houzet
 */
public class FtpCommands {

    String user;
    String password;
    Connexion connexion;
    InputStream in;
    OutputStream out;
    PrintWriter sendCommand;
    InputStreamReader isr;
    String pwd;
    BufferedReader reader;

    int dataPort;
    String dataHost;

    /***
     * Constructor of the FtpCommand class
     * @param user the username for the authentication to the ftp server
     * @param password the password for the authentication to the ftp server
     * @param ftpConnexion the Connexion object that contains the socket connected to the ftp server
     * @throws ErrorOfInitStreamException if they are a problem while initialising a stream
     */
    public FtpCommands(String user, String password, Connexion ftpConnexion) throws ErrorOfInitStreamException {
        this.connexion = ftpConnexion;
        this.password = password;
        this.user = user;
        this.in = this.connexion.InitInputStream();
        this.out = this.connexion.InitOutputStream();
        this.sendCommand = new PrintWriter(this.out, true);
        this.isr = new InputStreamReader(this.in);
        this.reader= new BufferedReader(this.isr);
    }

    /***
     * Method used to authenticate the user in the FTP server.
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     */
    public void LoginToFtpServer() throws ErrorOfSystemReadingException {
        sendCommand.println("USER " + this.user + "\r");
        this.ReceiveProcedure();
        sendCommand.println("PASS " + this.password + "\r");
        this.ReceiveProcedure();


    }

    /***
     *Method used to receive the server output
     * @return the received message from the ftp server.
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     */
    public String ReceiveProcedure() throws  ErrorOfSystemReadingException {

        try {
            return reader.readLine();
        }catch (IOException e){
            throw new ErrorOfSystemReadingException("Erreur lors de la lecture du Input stream");
        }
    }

    /***
     * Used to move inside the FTP server
     * @param directory the name of the directory to go in
     * @return the output of the server after have sent the command
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     */
    public String ChangeWordingDirectory(String directory) throws ErrorOfSystemReadingException {
        //System.out.println("CWD " + directory + "\r");
        sendCommand.println("CWD " + directory + "\r");
        return (ReceiveProcedure());
    }

    /***
     * Used to move in the upside directory into the FTP server
     * @return the output of the server after have sent the command
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     */
    public String ChangeParentDirectory() throws ErrorOfSystemReadingException {
        sendCommand.println("CDUP\r");
        return (ReceiveProcedure());
    }

    /***
     * Used to know the current working directory into the FTP server
     * @return the current working directory into the FTP server
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     */
    public String PWD() throws ErrorOfSystemReadingException {
        sendCommand.println("PWD\r");
        return (ReceiveProcedure());
    }

    /***
     * Used to set the server in a passive mode.
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     * @throws ErrorWhileCreatingSocketException  if they is an error while creating the data socket
     */
    public void EnterPassiveMode() throws  ErrorOfSystemReadingException, ErrorWhileCreatingSocketException {
        sendCommand.println("PASV\r");
        String res = ReceiveProcedure();
        String res1 = res.substring(27, res.length() - 2);
        String[] tab;
        tab = res1.split(",");

        this.dataHost="";
        for(int i=0;i<4;i++) {
            this.dataHost += tab[i];
            if (i!=3){
                this.dataHost +=".";
            }
        }
        this.dataPort = Integer.parseInt(tab[4]) * 256 + Integer.parseInt(tab[5]);
    }

    /***
     * Method that create a tree to represent the architecture of the FTP server.
     * @param indent the current indent if the program.
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     * @throws ErrorOfInitStreamException if they are a problem while initialising a stream
     * @throws ErrorWhileCreatingSocketException if they are an error while passing in passive mode
     * @throws IOException if they are an error while reading the data port
     */
    public void Tree (String indent) throws ErrorOfSystemReadingException, ErrorOfInitStreamException, ErrorWhileCreatingSocketException, IOException {
        String res = this.PWD();
        if (res == null) {
            reconnection();
        } else {
            this.EnterPassiveMode(); //lance une nouvelle connexion passive
            Connexion dataConnection = new Connexion(this.dataHost,dataPort);
            sendCommand.println("LIST\r");//effectue la commande LIST
            res=this.ReceiveProcedure();
            String x =res;

            InputStream inData = dataConnection.InitInputStream();//recupère le port de données
            InputStreamReader isr = new InputStreamReader(inData);
            BufferedReader reader = new BufferedReader(isr);
            String content = reader.readLine();// lis la première ligne de list


            while (content != null) { // temps que LIST ne renvoie pas null, on regarde si c'est un dossier, lien ou autre
                switch (content.charAt(0)) {

                    case ('d'): // si c'est un dossier
                        String[] tmpTab = content.split(" ");
                        System.out.println(indent + tmpTab[tmpTab.length - 1]);// on affiche indentation + le dossier courant
                        res = this.ChangeWordingDirectory(tmpTab[tmpTab.length - 1]);// on va dans le dossier
                        res = ReceiveProcedure();
                        if ( res != null && !(res.startsWith("550"))) {
                            Tree(indent = indent + "|--");// appel récursif a tree
                            res = this.ChangeParentDirectory();// on remonte
                            res = this.PWD();
                            indent = indent.substring(0, indent.length() - 3);
                        }
                        res = this.PWD();
                        break;

                    case ('l'):
                        tmpTab = content.split(" ");
                        System.out.println(indent + tmpTab[tmpTab.length - 3]);// si c'est un lien on affiche indentation+ son nom
                        break;

                    default:
                        tmpTab = content.split(" ");// si c'est un dossier on affiche indentation+ son nom
                        System.out.println(indent + tmpTab[tmpTab.length - 1]);
                }
                content = reader.readLine();
            }
            dataConnection.socket.close();
            if (x.startsWith("550")){
                this.ReceiveProcedure();}

        }

    }


    /***
     * From the string receive by the command PWD, return the current repertory exact
     * @param chemin the full current repertory string
     * @return the current repertory
     */
    public String givePath(String chemin) {
        String[] tmp = chemin.split(" ");
        return tmp[1].substring(1,tmp[1].length()-1);
    }

    /***
     * Method called if they is a lost of connexion with the server during the tree making
     * @throws ErrorOfInitStreamException  if they are a problem while initialising a stream
     * @throws ErrorOfSystemReadingException if they is an error while receiving server instructions
     * @throws ErrorWhileCreatingSocketException if they are a problem while re creating the socket
     */
    public void reconnection() throws ErrorOfInitStreamException, ErrorOfSystemReadingException, ErrorWhileCreatingSocketException {

        System.out.println("En reconnexion");
        /* recree la socket, le reader et printer + refaire la connexion et apres replacer au bon endroit*/

        this.connexion= new Connexion(connexion.host, connexion.port);
        this.in = this.connexion.InitInputStream();
        this.out = this.connexion.InitOutputStream();
        this.sendCommand = new PrintWriter(this.out, true);
        this.isr = new InputStreamReader(this.in);
        this.reader= new BufferedReader(this.isr);
        LoginToFtpServer();
        ChangeWordingDirectory(pwd);
    }

}


