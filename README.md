Tree FTP  
Nathan Houzet  
2021

# Introduction 

L'objectif du projet est de mettre en œuvre un commande shell permettant d'afficher sur la sortie standard d'un terminal  l'arborescence d'un répertoire distant accessible via le protocole applicatif File Transfer Protocol (FTP). Le rendu de l'arborescence distante s'inspirera du formalisme utilisé la commande tree de Linux.

# Lancer le programme

Une [démo du programme](./doc/demoTreeFtp.mp4) est disponible dans le dossier doc.

Pour lancer le programme il faut : 
- se placer à la racine du projet dans le dossier "projet1"
- executer la commande `mvn package` ( si tout est bien configuré, un dossier target avec un executable "tree-ftp-1.jar" sont générés )
- executer le jar avec la commande `java -jar target/tree-ftp-1.jar <serveur-ftp> <utilisateur> <mot-de-passe>`

*Remarque 1: Si la commande `mvn package` echoue (erreur due à une mauvaise version de maven par rapport à celle utilisé pour la création du projet), l'archive `tree-ftp-1.jar` est disponible dans le dossier "doc".*

*Remarque 2: Les champs `utilisateur` et `mot-de-passe` sont optionnels. Si il ne sont pas ajouté à la commande, l'application essayera de se connecter au serveur ftp en mode anonyme.*


# Architechture

Le programme est principalement composé de 3 classes.



**Une classe Connexion** : 

Cette classe gère la connexion avec le serveur ftp. En effet c'est dans cette classe que la socket est créée ainsi que les flux d'entrée et de sortie pour communiquer avec le serveur.

**Une classe FtpCommands** : 

Cette classe gère la communication avec le serveur ftp. Elle implémante toute les commandes usuelles et utiles pour naviguer sur le serveur. Cela comprends l'authentification mais également la navigation sur le serveur ( CWD, CDUP, PWD).  
La classe gère également la réception de messages envoyé par le serveur. C'est utile pour connaitre si les commandes ci-dessus ont réussies ou échoués par exemple.  
Cette classe permet aussi de paramétrer le serveur ftp en mode passif afin d'y recevoir des flux de données.  
Ce dernier flux de données est très largement utilisé pour réaliser la fonctionnalité principale de cette classe et du programme : La commande Tree.

Diagrammes UML des classes Connexion et FtpCommands : 

![Classes crées.](./doc/UML.png)

**La classe Main** : 

Cette classe permet de récuperer les arguments et lancer les méthodes permettant de se connecter au serveur via les paramètres reçus. Une fois la connexion établie , la classe execute la commande tree sur le serveur ftp distant.

**Les exeptions** : 

De plus, trois exceptions ont été créés pour préciser les différentes pannes qu'il peut y avoir lors de l'éxecution du programme : 

- Une exception qui concerne la socket, ci celle-ci n'arrive pas à se connecter à l'hote via le port voulu, alors l'exception "ErrorWhileCreatingSocketException" est levé.

- Une exeption qui concerne les flux pour communiquer avec le serveur. Si une erreur apparait lors de la création du flux d'entrée ou de sortie du serveur FTP alors l'exception "ErrorOfInitStreamException" est levé.

- La dernière exeption concerne la lecture du flux de réponse du serveur. Comme pour chaque commande envoyé au serveur génére une réponse, il est alors intéressant de créer une exeption si jamais une erreur se produit lors de la lecture de la réponse. Il d'agit de l'exeption "ErrorOfSystemReadingException".

*NB*  
La fonctionnalité de reprise sur interrupion à été implémenté. Elle se situe au niveau de la fonction `Reconnection` dans la classe FtpCommands.

# Parcours de code

**Code 1**
```java
public void EnterPassiveMode() throws  ErrorOfSystemReadingException, ErrorWhileCreatingSocketException {
        sendCommand.println("PASV\r");
        String res = ReceiveProcedure();
        String res1 = res.substring(27, res.length() - 2);
        String[] tab;
        tab = res1.split(",");

        this.dataHost="";
        for(int i=0;i<4;i++) {
            this.dataHost += tab[i];
            if (i!=3){
                this.dataHost +=".";
            }
        }
        this.dataPort = Integer.parseInt(tab[4]) * 256 + Integer.parseInt(tab[5]);
```

Cette fonction est la fonction qui permet de configurer le mode passif du serveur ftp.
Le mode passif consiste à demander une adresse et un port au serveur ftp pour y recevoir les données d'une commande dont le résultat est assez lourd.Comme la commande `LIST` qui permet de lister le contenu d'un répertoire 

Elle se compose de trois étapes principales :
- la première envoie la commande `PASV ` au serveur pour lui signifier que l'on veut passer en mode passif. 
- la seconde étape consiste à récupérer la chaine répondu par le serveur.
- la troisième étape sert à récupérer l'adresse de connexion pour recevoir les donnée et à calculer le port utilisé par le serveur.

Par la suite une socket est connecté à cette adresse et à ce port pour récupérer les données transmisent par le serveur.

**Code 2**
```java
public void Tree (String indent) ...{
       ...
        //Passage du serveur en mode passif et envoie la commande LIST 
        //puis récupère son résulat ligne part ligne.
       ...
         while (content != null) { // temps que LIST ne renvoie pas null, on regarde si c'est un dossier, lien ou autre
                switch (content.charAt(0)) {

                    case ('d'): // si c'est un dossier
                        String[] tmpTab = content.split(" ");
                        System.out.println(indent + tmpTab[tmpTab.length - 1]);// on affiche indentation + le dossier courant
                        res = this.ChangeWordingDirectory(tmpTab[tmpTab.length - 1]);// on va dans le dossier
                        res = ReceiveProcedure();
                        if ( res != null && !(res.startsWith("550"))) {
                            Tree(indent = indent + "|--");// appel récursif a tree
                            res = this.ChangeParentDirectory();// on remonte
                            res = this.PWD();
                            indent = indent.substring(0, indent.length() - 3);
                        }
                        res = this.PWD();
                        break;

                    case ('l'):
                        tmpTab = content.split(" ");
                        System.out.println(indent + tmpTab[tmpTab.length - 3]);// si c'est un lien on affiche indentation+ son nom
                        break;

                    default:
                        tmpTab = content.split(" ");// si c'est un dossier on affiche indentation+ son nom
                        System.out.println(indent + tmpTab[tmpTab.length - 1]);
                }
                content = reader.readLine();
            }
            dataConnection.socket.close();
            if (x.startsWith("550")){
                this.ReceiveProcedure();}

        }

    }
```

Il s'agit ici du code de la fonction Tree qui permet d'afficher l'arbre.  
La technique principale à été pour chaque type de fichiers, les différencier selon leur types : 
- si c'est un fichier.
- si c'est un lien.
- si c'est un dossier.

Ainsi pour un fichier, il faut alors afficher l'indentation correspondante à la profondeur actuelle et son nom.  
Pour un lien, faire la même chose en fesant attention de ne pas afficher l'adresse du lien mais bien le lien en lui même.  
Et pour un dossier, se déplacer dans le dossier et faire un appel récursif à la fonction Tree.


# Tests

Il n'a pas été facile de réaliser des tests car chaque fonction néssécite d'avoir un serveur connecté pour analyser et testé la réponse voulue.

Un seul test à donc été réalisé. Il permet de vérifier que si le serveur en paramètre de la commande est mauvais, alors l'exception associé se déclenche bien.

# Documentation

La documenation est disponible dans le dossier doc via le fichier [*index.html*](./doc/index.html) situé dans le fichier doc. 